<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Struts - Demo - Login Form Using Hibernate</title>
</head>
<body>
<h1>Login Form:</h1>
<form action="process-login.html" method="post">
    <table>
        <tr>
            <td>Username:</td>
            <td><input type="text" name="username"></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type="password" name="password"></td>
        </tr>
        <tr>
            <td><input type="submit" value="Login"></td>
        </tr>
    </table>
</form>
<%
    String message = (String) request.getAttribute("message");
    if (!(message == null)) {
        out.println(message);
    } else {
        message = "";
        out.println(message);
    }
%>
</body>
</html>