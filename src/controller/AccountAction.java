package controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import model.AccountModel;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import util.EncryptUtil;

import com.opensymphony.xwork2.*;

import entities.Account;

public class AccountAction extends ActionSupport implements SessionAware, ServletRequestAware {

    private static final long serialVersionUID = 1L;

    private Map<String, Object> session;
    private HttpServletRequest request;

    private AccountModel accountModel = new AccountModel();

    private List<Account> lstAccounts;

    private String username;
    private String password;

    public List<Account> getLstAccounts() {
        return lstAccounts;
    }

    public void setLstAccounts(List<Account> lstAccounts) {
        this.lstAccounts = lstAccounts;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Map<String, Object> getSession() {
        return session;
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    // 1. Display login form (login.jsp).
    @Override
    public String execute() throws Exception {
        System.out.println("Login action start");
        return "login";
    }

    // 2. Process login form.
    public String processLogin() throws Exception {
        System.out.println("Login process");
        this.lstAccounts = accountModel.getAccounts();
        for (Account account : lstAccounts) {
            if (account.getUsername().trim().equals(this.username)
                    && account.getPassword().trim().equals(EncryptUtil.md5(this.password))) {
                this.session.put("username", account.getUsername());
                return SUCCESS;
            }
        }
        request.setAttribute("message", "Username or password is invalid!");
        return ERROR;
    }

    // 3. Process logout.
    public String logout() throws Exception {
        System.out.println("Logout process");
        if (this.session.containsKey("username")) {
            this.session.remove("username");
        }
        return SUCCESS;
    }
}