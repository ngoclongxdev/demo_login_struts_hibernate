package model;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import util.HibernateUtil;
import entities.Account;

/**
 * @author Phuong Do
 */
public class AccountModel {

    // 1. Get all accounts.
    public List<Account> getAccounts() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM Account");
            List<Account> lstAccounts = query.list();
            transaction.commit();
            return lstAccounts;
        } catch (Exception e) {
            if (!(transaction == null)) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return null;
    }
}